/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80034 (8.0.34)
 Source Host           : localhost:3306
 Source Schema         : mall

 Target Server Type    : MySQL
 Target Server Version : 80034 (8.0.34)
 File Encoding         : 65001

 Date: 06/06/2024 09:28:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `age` tinyint NULL DEFAULT NULL COMMENT '管理员的年龄',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '管理员的手机号',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '管理员的邮箱',
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '管理员的地址',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '管理员账户的创建时间',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '管理员信息的最后更新时间',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '管理员的头像URL',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '管理员的个人描述',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '管理员账户的删除状态，0表示未删除，1表示已删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (3, 'admin3', '2309433bcb3d8c60ce6e169f0b885596', '5860c41b-b0e0-43c4-a41b-ee0c9a1f3b13', NULL, NULL, NULL, NULL, '2024-05-12 18:00:04', '2024-05-12 18:00:04', NULL, NULL, 0);
INSERT INTO `admin` VALUES (4, 'admin', 'b9fe98b69b0be2041cd2c37c21543798', '5778ca95-2e49-4935-beb3-eab1949284af', 20, '19522376658', '123@126.com', '郑州市中原区中原工学院', '2024-05-12 18:00:04', '2024-05-12 20:39:01', 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/admin.jpg', NULL, 0);
INSERT INTO `admin` VALUES (9, 'liyunlong', 'c40afaa270c4d6920012c402bb0720f5', '5fae2e02-b5ae-49ac-8fe9-b7f555a3eb68', NULL, NULL, NULL, NULL, '2024-06-05 18:43:11', '2024-06-05 18:43:11', NULL, NULL, 0);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '订单唯一标识',
  `product_id` int NOT NULL COMMENT '关联的商品ID',
  `quantity` int NOT NULL COMMENT '订单中的商品数量',
  `order_date` datetime NOT NULL COMMENT '下单时间',
  `user_id` int NOT NULL COMMENT '购买用户ID',
  `status` tinyint(1) NOT NULL COMMENT '订单状态：01 - 待处理，02 - 处理中，03 - 已完成，04 - 已取消',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除标志：0 - 未删除，1 - 已删除',
  `unit_price` decimal(10, 2) NOT NULL COMMENT '商品单价',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '存储所有订单信息的表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 1, 20, '2024-05-12 20:53:24', 1, 3, 0, 5999.00);
INSERT INTO `orders` VALUES (2, 1, 2, '2024-06-12 20:54:23', 1, 3, 0, 5999.00);
INSERT INTO `orders` VALUES (3, 2, 3, '2024-07-12 20:54:51', 2, 3, 0, 19999.00);
INSERT INTO `orders` VALUES (4, 2, 6, '2024-04-12 20:55:29', 2, 3, 0, 19999.00);
INSERT INTO `orders` VALUES (5, 3, 8, '2024-03-12 20:56:18', 1, 3, 0, 7999.00);
INSERT INTO `orders` VALUES (6, 1, 1, '2024-03-12 22:15:15', 3, 3, 0, 5999.00);
INSERT INTO `orders` VALUES (7, 4, 44, '2024-05-14 17:03:45', 1, 3, 0, 1969.00);
INSERT INTO `orders` VALUES (8, 5, 66, '2024-05-14 17:30:10', 2, 3, 0, 1999.00);
INSERT INTO `orders` VALUES (9, 12, 552, '2024-05-14 17:31:01', 2, 3, 0, 699.00);
INSERT INTO `orders` VALUES (10, 16, 33, '2024-05-14 20:46:02', 2, 3, 0, 4999.00);
INSERT INTO `orders` VALUES (11, 17, 345, '2024-05-14 20:46:39', 1, 3, 0, 6775.00);
INSERT INTO `orders` VALUES (12, 18, 546, '2024-06-14 20:53:45', 1, 3, 0, 19999.00);

-- ----------------------------
-- Table structure for product_views
-- ----------------------------
DROP TABLE IF EXISTS `product_views`;
CREATE TABLE `product_views`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '访问量记录唯一标识',
  `product_id` int NOT NULL COMMENT '商品唯一标识，与products表中的id关联',
  `view_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '访问时间，默认为当前时间',
  `views_count` int NOT NULL COMMENT '该次访问的访问量',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_id`(`product_id` ASC) USING BTREE,
  CONSTRAINT `product_views_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '存储每个商品每次访问量的表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_views
-- ----------------------------
INSERT INTO `product_views` VALUES (1, 1, '2024-03-12 21:33:53', 3000);
INSERT INTO `product_views` VALUES (2, 2, '2024-04-12 21:34:29', 20);
INSERT INTO `product_views` VALUES (3, 3, '2024-03-12 21:34:50', 5000);
INSERT INTO `product_views` VALUES (4, 4, '2024-04-12 21:34:59', 200);
INSERT INTO `product_views` VALUES (5, 5, '2024-05-12 21:35:09', 342);
INSERT INTO `product_views` VALUES (10, 12, '2024-05-14 20:47:12', 343);
INSERT INTO `product_views` VALUES (11, 16, '2024-04-14 20:47:25', 33);
INSERT INTO `product_views` VALUES (12, 18, '2024-04-14 20:54:35', 435);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '商品唯一标识',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '产品名称',
  `price` decimal(10, 2) NOT NULL COMMENT '商品售价',
  `stock` int NOT NULL COMMENT '库存数量',
  `status` tinyint(1) NOT NULL COMMENT '商品状态：01 - 上架，00 - 下架',
  `views` int NOT NULL COMMENT '商品页面被查看次数',
  `launch_date` date NOT NULL COMMENT '商品首次上架日期',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '商品详细描述',
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除标志：0 - 未删除，1 - 已删除',
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品图片URL',
  `sales_volume` int NOT NULL DEFAULT 0 COMMENT '销售量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '存储所有商品信息的表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'iphoneXsMax', 5999.00, 4002, 1, 233, '2024-05-11', '苹果', 0, 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/4068ea34-9e79-4908-937b-58fb01b0b7aa.png', 6000);
INSERT INTO `products` VALUES (2, 'MacBookPro', 19999.00, 23432, 0, 43543, '2024-05-11', '苹果mac', 0, 'https://sky-liyunlong.oss-cn-beijing.aliyuncs.com/mac.png', 5000);
INSERT INTO `products` VALUES (3, 'iPadPro', 7999.00, 20, 0, 4000, '2024-05-12', 'iPadPro2024', 0, 'https://sky-liyunlong.oss-cn-beijing.aliyuncs.com/ipad.jpg', 1000);
INSERT INTO `products` VALUES (4, 'iWatch', 1969.00, 4555, 1, 9000, '2024-05-12', 'iWatch S8', 0, 'https://sky-liyunlong.oss-cn-beijing.aliyuncs.com/watch.jpg', 700);
INSERT INTO `products` VALUES (5, 'Airpods', 1999.00, 23, 1, 20, '2024-05-12', '中文名苹果无线耳机\n外文名Apple AirPods\n上市时间2016年9月8日\n所属品牌苹果\n产品类型无线蓝牙耳机\n特    征内置红外传感器\n售    价159美元（约为1010.7人民币）\n续    航5小时', 0, 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/airpods.jpg', 80);
INSERT INTO `products` VALUES (12, 'Home Pod', 699.00, 566, 1, 0, '2024-05-13', 'Home pad mni 一代', 0, 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/158ed9d0-261d-4907-8e93-4aee79e52a5f.jpg', 0);
INSERT INTO `products` VALUES (16, 'Xiaomi 11Pro', 2399.00, 600, 1, 0, '2024-05-14', 'Xiaomi 11Pro', 0, 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/02729ed7-8af7-44e8-97c4-d8d9b2f5828e.jpg', 0);
INSERT INTO `products` VALUES (17, 'Xiaomi 11Utra', 5999.00, 599, 0, 0, '2024-05-14', '安卓之光', 0, 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/81b3dfd3-34e5-45eb-b9e9-8260d6c1bb00.jpg', 0);
INSERT INTO `products` VALUES (18, 'Vision Pro', 29999.00, 122, 1, 0, '2024-05-14', 'Apple Vision Pro 2024', 0, 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/05d0fab6-ee50-430c-96ee-21232e0230bc.jpg', 0);
INSERT INTO `products` VALUES (19, 'test', 2.00, 200, 1, 0, '2024-06-06', 'test商品', 0, 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/53340cc5-b05b-4164-8af9-68756e739ddf.png', 0);

-- ----------------------------
-- Table structure for source
-- ----------------------------
DROP TABLE IF EXISTS `source`;
CREATE TABLE `source`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `source_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `source_name`(`source_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of source
-- ----------------------------
INSERT INTO `source` VALUES (6, '抖音平台');
INSERT INTO `source` VALUES (5, '校园平台');
INSERT INTO `source` VALUES (1, '游戏平台');
INSERT INTO `source` VALUES (4, '电商平台');
INSERT INTO `source` VALUES (2, '视频娱乐');
INSERT INTO `source` VALUES (3, '门户媒体');

-- ----------------------------
-- Table structure for user_source
-- ----------------------------
DROP TABLE IF EXISTS `user_source`;
CREATE TABLE `user_source`  (
  `user_id` int NOT NULL,
  `source_id` int NOT NULL,
  PRIMARY KEY (`user_id`, `source_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_source
-- ----------------------------
INSERT INTO `user_source` VALUES (1, 1);
INSERT INTO `user_source` VALUES (2, 2);
INSERT INTO `user_source` VALUES (25, 6);
INSERT INTO `user_source` VALUES (26, 5);
INSERT INTO `user_source` VALUES (27, 1);
INSERT INTO `user_source` VALUES (28, 4);
INSERT INTO `user_source` VALUES (29, 3);

-- ----------------------------
-- Table structure for user_visits
-- ----------------------------
DROP TABLE IF EXISTS `user_visits`;
CREATE TABLE `user_visits`  (
  `user_id` int NOT NULL COMMENT '用户ID',
  `product_id` int NOT NULL COMMENT '商品ID',
  `visit_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '访问时间',
  `visits_count` int NULL DEFAULT 1 COMMENT '当天访问次数',
  PRIMARY KEY (`user_id`, `product_id`, `visit_time`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户访问记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_visits
-- ----------------------------
INSERT INTO `user_visits` VALUES (1, 1, '2024-05-13 21:27:49', 23);
INSERT INTO `user_visits` VALUES (1, 2, '2024-05-12 21:29:20', 43);
INSERT INTO `user_visits` VALUES (2, 2, '2024-05-11 21:32:30', 100);
INSERT INTO `user_visits` VALUES (2, 2, '2024-05-15 21:28:06', 3);
INSERT INTO `user_visits` VALUES (2, 3, '2024-05-17 22:04:13', 324);
INSERT INTO `user_visits` VALUES (3, 2, '2024-05-12 21:59:25', 423);
INSERT INTO `user_visits` VALUES (3, 3, '2024-05-11 21:29:42', 33);
INSERT INTO `user_visits` VALUES (3, 3, '2024-05-14 21:28:27', 3);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `age` tinyint NOT NULL,
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `address` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username` ASC) USING BTREE,
  UNIQUE INDEX `mobile`(`mobile` ASC) USING BTREE,
  UNIQUE INDEX `email`(`email` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '李云龙', 23, '19522376658', '123@qq.com', '中原工大学', '2024-05-10 21:32:54', '2024-05-13 23:01:10', 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/4ea74ea0-858e-4de6-97bf-ca6efc90c569.jpg', '亮剑', 0);
INSERT INTO `users` VALUES (2, '张三', 33, '13183239518', '1234@qq.com', '沙雕', '2024-05-10 21:33:35', '2024-05-14 20:55:28', NULL, '牛逼王', 1);
INSERT INTO `users` VALUES (25, 'dad1222wr', 22, '1902549586423', '3213321334243432', '中原区中原工学院', '2024-05-11 19:23:14', '2024-05-14 12:47:53', NULL, NULL, 0);
INSERT INTO `users` VALUES (26, 'dad1222wr43', 22, '1902549586423534', '3213321334243432543', '中原区中原工学院', '2024-05-11 19:27:03', '2024-05-14 12:47:53', NULL, NULL, 0);
INSERT INTO `users` VALUES (27, 'dad1222wr433', 22, '1902549586423534342', '3213321334243432543423', '中原区中原工学院', '2024-05-11 19:27:42', '2024-05-14 12:47:53', NULL, NULL, 0);
INSERT INTO `users` VALUES (28, 'admin', 22, '195223766583', '3231029983@qq.com', '中原区中原工学院', '2024-05-11 19:29:23', '2024-05-14 17:26:07', NULL, NULL, 1);
INSERT INTO `users` VALUES (29, '刘浩存', 21, '19522336658', '12321@qq.com', '我家', '2024-05-13 13:16:15', '2024-05-13 23:01:10', 'https://liyunlong-mall.oss-cn-beijing.aliyuncs.com/319f5a5f-460b-4fd9-be1f-4ab1ce62fba5.jpg', 'wo', 0);

SET FOREIGN_KEY_CHECKS = 1;
