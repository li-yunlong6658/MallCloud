package com.lyl.mallcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/***
 * @Description: 启动入口
 * @Author: Liyunlong
 * @Date: 2024/5/10
 */
@SpringBootApplication
public class MallCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallCloudApplication.class, args);
        System.out.println("Mall启动成功>>>>>>>>>>>>>>>>>>>>>>");
    }

}
