package com.lyl.mallcloud.mapper;

import com.lyl.mallcloud.pojo.ProductViews;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyl.mallcloud.pojo.VisitDatum;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 存储每个商品每次访问量的表 Mapper 接口
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-12
 */
@Mapper
public interface ProductViewsMapper extends BaseMapper<ProductViews> {


    Integer getViewsCountSum(Integer productId);

    List<Map<String, Object>> countVisits();

    List<VisitDatum> getWeeklyVisitData();
}
