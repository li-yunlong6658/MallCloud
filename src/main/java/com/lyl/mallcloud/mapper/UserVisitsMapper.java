package com.lyl.mallcloud.mapper;

import com.lyl.mallcloud.pojo.UserVisits;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyl.mallcloud.pojo.VisitDatum;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 用户访问记录表 Mapper 接口
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */

@Mapper
public interface UserVisitsMapper extends BaseMapper<UserVisits> {

}
