package com.lyl.mallcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyl.mallcloud.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;

/**

 * @author liyunlong
 * @since 2024-05-10
 */

@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

    Admin findByUsername(String username);
}
