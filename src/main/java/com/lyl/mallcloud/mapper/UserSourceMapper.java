package com.lyl.mallcloud.mapper;

import com.lyl.mallcloud.pojo.UserSource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */

@Mapper
public interface UserSourceMapper extends BaseMapper<UserSource> {

    List<Map<String, Object>> selectBySource();
}
