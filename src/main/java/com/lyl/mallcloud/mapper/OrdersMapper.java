package com.lyl.mallcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyl.mallcloud.pojo.Orders;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 存储所有订单信息的表 Mapper 接口
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-12
 */

@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {

    long moneySum();

    long productSum();

    List<Map<String, Object>> getTopFiveProductIdsBySalesVolume();
}
