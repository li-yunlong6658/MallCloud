package com.lyl.mallcloud.mapper;

import com.lyl.mallcloud.pojo.Source;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */
@Mapper
public interface SourceMapper extends BaseMapper<Source> {

}
