package com.lyl.mallcloud.mapper;

import com.lyl.mallcloud.pojo.Products;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 存储所有商品信息的表 Mapper 接口
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-11
 */

@Mapper
public interface ProductsMapper extends BaseMapper<Products> {

    void updateStatusById(Products id1);

    long productCount();



}
