package com.lyl.mallcloud.mapper;

import com.lyl.mallcloud.pojo.Users;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-10
 */

@Mapper
public interface UsersMapper extends BaseMapper<Users> {

    int userCount();

    Users findByUserName(String name);
}
