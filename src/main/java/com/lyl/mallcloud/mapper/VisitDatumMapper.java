package com.lyl.mallcloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lyl.mallcloud.pojo.UserVisits;
import com.lyl.mallcloud.pojo.VisitDatum;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: Liyunlong
 * @Date: 2024/05/14/21:51
 * @Description:
 */
@Mapper
public interface VisitDatumMapper extends BaseMapper<UserVisits> {
    List<VisitDatum> getWeeklyVisitData();

}