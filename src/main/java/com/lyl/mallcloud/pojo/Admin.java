package com.lyl.mallcloud.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-12
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("admin")
public class Admin implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String username;

    private String password;

    private String salt;

    /**
     * 管理员的年龄
     */
    private Integer age;

    /**
     * 管理员的手机号
     */
    private String mobile;

    /**
     * 管理员的邮箱
     */
    private String email;

    /**
     * 管理员的地址
     */
    private String address;

    /**
     * 管理员账户的创建时间
     */
    private LocalDateTime createdAt;

    /**
     * 管理员信息的最后更新时间
     */
    private LocalDateTime updatedAt;

    /**
     * 管理员的头像URL
     */
    private String avatar;

    /**
     * 管理员的个人描述
     */
    private String description;

    /**
     * 管理员账户的删除状态，0表示未删除，1表示已删除
     */
    private Boolean isDeleted;

}
