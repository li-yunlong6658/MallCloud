package com.lyl.mallcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/***
* @Description: 工作台数据收集
* @Author: Liyunlong
* @Date: 2024/5/13
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkStats {
       // 会员数量
       private Long userCount;
       // 平台商品数量
       private Long productCount;
       // 商品销售总量来源订单表
       private Long productSum;
       // 总销售进而
       private Long money;
       // 用户在线数量 这里实际统计的 管理员在线数量
       private Long vipCount;

   }
   