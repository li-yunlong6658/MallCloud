package com.lyl.mallcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Liyunlong
 * @Date: 2024/05/12/15:29
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VisitCount {
    private String name;
    private Long value;

}
