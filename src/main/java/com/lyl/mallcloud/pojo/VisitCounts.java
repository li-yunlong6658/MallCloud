package com.lyl.mallcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author: Liyunlong
 * @Date: 2024/05/12/15:29
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class VisitCounts {
    private List<String> legendData;
    private List<VisitCount> data;
}
