package com.lyl.mallcloud.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("users")
public class Users implements Serializable {

    private static final long serialVersionUID = 1L;
    @Excel(name = "用户ID", width = 10)
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @Excel(name = "用户名", width = 10,needMerge = true)
    private String username;
    @Excel(name = "年龄", width = 10)
    private Integer age;
    @Excel(name = "手机号", width = 20, needMerge = true)
    private String mobile;
    @Excel(name = "邮箱", width = 20, needMerge = true)
    private String email;
    @Excel(name = "地址", width = 20, needMerge = true)
    private String address;
    @Excel(name ="注册时间", width = 20, format = "yyyy-MM-dd")
    @JsonIgnore
    private LocalDateTime createdAt;
    @Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd")
    @JsonIgnore
    private LocalDateTime updatedAt;

    @JsonIgnore
    private String avatar;
    @Excel(name = "会员描述", width = 20, needMerge = true)
    @JsonIgnore
    private String description;
    @TableLogic
    private Integer isDeleted;

}
