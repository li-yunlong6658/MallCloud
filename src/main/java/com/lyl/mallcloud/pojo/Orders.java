package com.lyl.mallcloud.pojo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 存储所有订单信息的表
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("orders")
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 订单唯一标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 关联的商品ID
     */
    private Integer productId;

    /**
     * 订单中的商品数量
     */
    private Integer quantity;

    /**
     * 下单时间
     */
    private LocalDateTime orderDate;

    /**
     * 购买用户ID
     */
    private Integer userId;

    /**
     * 订单状态：01 - 待处理，02 - 处理中，03 - 已完成，04 - 已取消
     */
    private Boolean status;

    /**
     * 逻辑删除标志：0 - 未删除，1 - 已删除
     */
    private Boolean isDeleted;

    /**
     * 商品单价
     */
    private BigDecimal unitPrice;


}
