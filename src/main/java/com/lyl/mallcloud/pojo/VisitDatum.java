package com.lyl.mallcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @Author: Liyunlong
 * @Date: 2024/05/14/21:20
 * @Description: 用户访问量 数据
 * visitTime：近七天时间
 * visits：每一天的总访问量
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VisitDatum {
    private String visitTime;
    private int visits;
}
