package com.lyl.mallcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author: Liyunlong
 * @Date: 2024/05/14/18:09
 * @Description:  用户来源接收类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserMint {
    private List<String> legendData;
    private List<VisitCount> data;
}
