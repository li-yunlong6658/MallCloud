package com.lyl.mallcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Liyunlong
 * @Date: 2024/05/12/14:26
 * @Description:  top5商品
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChartData {
    private String[] categories;
    private int[] visits;
    private int[] stock;
}
