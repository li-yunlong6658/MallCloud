package com.lyl.mallcloud.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String productName;
    private BigDecimal price;
    private Integer stock;
    private transient MultipartFile imageUrl; // 使用Spring框架的MultipartFile处理文件上传
    private String description;

}
