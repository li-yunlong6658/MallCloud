package com.lyl.mallcloud.pojo;

import java.math.BigDecimal;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 存储所有商品信息的表
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("products")
public class Products implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 商品唯一标识
     */
    @Excel(name = "商品ID", width = 10)
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 产品名称
     */
    @Excel(name = "商品名称", width = 20,needMerge = true)
    private String productName;

    /**
     * 商品售价
     */
    @Excel(name = "商品售价", width = 10)
    private BigDecimal price;

    /**
     * 库存数量
     */

    @Excel(name = "商品库存", width = 10, suffix = "件")
    private Integer stock;

    /**
     * 商品状态：01 - 上架，00 - 下架
     */
    @Excel(name = "商品状态", width = 10)
    private Boolean status;

    /**
     * 商品页面被查看次数
     */
    @Excel(name = "商品浏览量", width = 15, suffix = "次")
    private Integer views;

    /**
     * 商品首次上架日期
     */
    private LocalDate launchDate;

    /**
     * 商品详细描述
     */
    @Excel(name = "商品描述", width = 10, needMerge = true)
    private String description;

    /**
     * 逻辑删除标志：0 - 未删除，1 - 已删除
     */

    @TableLogic
    private Boolean isDeleted;


    private String  imageUrl;
    @Excel(name = "销售量", width = 10, needMerge = true, suffix = "件")
    private Integer salesVolume;


}
