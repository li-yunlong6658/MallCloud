package com.lyl.mallcloud.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 存储每个商品每次访问量的表
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("product_views")
public class ProductViews implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 访问量记录唯一标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品唯一标识，与products表中的id关联
     */
    private Integer productId;

    /**
     * 访问时间，默认为当前时间
     */
    private LocalDateTime viewTime;

    /**
     * 该次访问的访问量
     */
    private Integer viewsCount;


}
