package com.lyl.mallcloud.pojo.page;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Liyunlong
 * @Date: 2024/04/29/21:54
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageParams {
    private Integer pageNum;
    // 记录数
    private Integer pageSize;

}
