package com.lyl.mallcloud.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lyl.mallcloud.mapper.OrdersMapper;
import com.lyl.mallcloud.mapper.ProductsMapper;
import com.lyl.mallcloud.mapper.UsersMapper;
import com.lyl.mallcloud.pojo.Orders;
import com.lyl.mallcloud.pojo.ProductDTO;
import com.lyl.mallcloud.pojo.Products;
import com.lyl.mallcloud.pojo.page.PageParams;
import com.lyl.mallcloud.service.IProductsService;
import com.lyl.mallcloud.utils.AliOssUtil;
import com.lyl.mallcloud.utils.ExcelUtils;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author liyunlong
 * @since 2024-05-11
 */
@Controller
@Slf4j
public class ProductsController {

    @Autowired
    ProductsMapper productsMapper;
    @Autowired
    IProductsService productsService;
    @Autowired
    UsersMapper usersMapper;
    @Autowired
    private AliOssUtil aliOssUtil;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    OrdersMapper ordersMapper;

    /***
     * @Description: 商品的分页查询 这里设置的是每页展示4条数据
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */

    @RequestMapping("/product/List")
    public String productList(@RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "4") Integer pageSize,
                              Model model, HttpSession session) {
        try {
            // 设置分页参数
            PageParams pageParams = new PageParams();
            pageParams.setPageNum(pageNum);
            pageParams.setPageSize(pageSize);
            PageHelper.startPage(pageParams.getPageNum(), pageSize);
            List<Products> products = productsMapper.selectList(null);
            PageInfo<Products> pageInfo = new PageInfo<>(products);
            int pages = pageInfo.getPages();

            List<Products> list = pageInfo.getList();
            model.addAttribute("currentPage", pageNum);
            model.addAttribute("pages", pages);
            model.addAttribute("productItemList", list);
            model.addAttribute("userName", session.getAttribute("userName"));
        } catch (Exception ignored) {
        }
        return "/product/List";
    }

    /***
     * @Description: 更新商品的状态，上下架
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @GetMapping("/updateStatus")
    public String delete(@RequestParam(value = "id") Long id, HttpSession session) {
        Products products = productsMapper.selectById(id);
        Boolean status = products.getStatus();
        if (status) {
            products.setStatus(false);
            productsMapper.updateById(products);
            session.setAttribute("msg", "商品：" + products.getProductName() + "，下架了");
            session.setAttribute("status", 0);
            log.info("商品：{}，下架了", products.getProductName());
        }
        if (!status) {
            products.setStatus(true);
            productsMapper.updateById(products);
            log.info("商品：{}，上架了", products.getProductName());
        }
        return "redirect:/product/List";
    }

    /***
     * @Description: 查看商品的详细信息
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @GetMapping("/Pinfo")
    public String info(@RequestParam(value = "id") Long id, Model model, HttpSession session) {
        Products products = productsMapper.selectById(id);
        model.addAttribute("product", products);
        model.addAttribute("userName", session.getAttribute("userName"));
        return "/product/Info";
    }

    /***
     * @Description: 数据看板 商品数据
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */

    @RequestMapping("/product/DataView")
    public String productDataView(Model model, HttpSession session) {
        try {
            QueryWrapper<Orders> salesQueryWrapper = new QueryWrapper<>();
            salesQueryWrapper.ge("quantity", 100);
            salesQueryWrapper.orderByDesc("quantity");
            List<Orders> salesOrderList = ordersMapper.selectList(salesQueryWrapper);
            Map<Integer, Long> productQuantityMap = salesOrderList.stream()
                    .collect(Collectors.groupingBy(Orders::getProductId,
                            Collectors.summingLong(Orders::getQuantity)));
            // 包含商品ID和总数量的列表
            List<Map.Entry<Integer, Long>> sortedEntries = new ArrayList<>(productQuantityMap.entrySet());
            // 按照总数量进行降序排序
            sortedEntries.sort(Map.Entry.<Integer, Long>comparingByValue().reversed());
            // 分别提取ID和数量，创建新的列表
            List<Integer> productIds = sortedEntries.stream()
                    .map(Map.Entry::getKey)
                    .toList();
            // 将所有商品的总数量放入一个集合
            List<Long> totalQuantities = sortedEntries.stream()
                    .map(Map.Entry::getValue)
                    .toList();
            // List<Products> salesProductList = productsMapper.selectBatchIds(productIds);
            List<Products> salesProductList = new ArrayList<>(productIds.size());
            for (Integer id : productIds) {
                Products product = productsMapper.selectById(id);
                if (product != null) {
                    salesProductList.add(product);
                }
            }
            // 查询按照库存排序的商品
            // 待补货 商品 限制是 库存量在 10-2000  之间的商品
            QueryWrapper<Products> stockQueryWrapper = new QueryWrapper<>();
            stockQueryWrapper.ge("stock", 10);
            stockQueryWrapper.le("stock", 200);
            stockQueryWrapper.orderByAsc("stock");
            List<Products> stockProductList = productsMapper.selectList(stockQueryWrapper);

            AtomicInteger salesRank = new AtomicInteger(1);
            List<Map<String, Object>> salesProductItemList = IntStream.range(0, productIds.size())
                    .boxed()
                    .map(i -> {
                        Products product = salesProductList.get(i);
                        // Integer productId = productIds.get(i);
                        Long totalQuantity = totalQuantities.get(i);
                        return createProductItemWithQuantity(product, salesRank.getAndIncrement(), /*productId,*/ totalQuantity);
                    })
                    .collect(Collectors.toList());

            AtomicInteger stockRank = new AtomicInteger(1);
            List<Map<String, Object>> stockProductItemList = stockProductList.stream()
                    .map(product -> createProductItem(product, stockRank.getAndIncrement()))
                    .collect(Collectors.toList());
            model.addAttribute("salesProductItemList", salesProductItemList);
            model.addAttribute("stockProductItemList", stockProductItemList);
            model.addAttribute("userName", session.getAttribute("userName"));
        } catch (Exception e) {
            // 处理异常
            throw new RuntimeException("查询商品数据失败", e);
        }
        return "/product/DataView";
    }

    private Map<String, Object> createProductItemWithQuantity(Products product, int rank, Long totalQuantity) {
        Map<String, Object> productItemInfo = new HashMap<>();
        productItemInfo.put("id", product.getId());
        productItemInfo.put("rank", rank);
        productItemInfo.put("productName", product.getProductName());
        productItemInfo.put("price", product.getPrice());
        productItemInfo.put("stock", product.getStock());
        productItemInfo.put("views", product.getViews());
        productItemInfo.put("launchDate", product.getLaunchDate());
        productItemInfo.put("salesVolume", product.getSalesVolume());
        productItemInfo.put("totalQuantity", totalQuantity);
        // productItemInfo.put("productId", productId);
        return productItemInfo;
    }

    private Map<String, Object> createProductItem(Products product, int rank) {
        Map<String, Object> productItemInfo = new HashMap<>();
        productItemInfo.put("id", product.getId());
        productItemInfo.put("rank", rank);
        productItemInfo.put("productName", product.getProductName());
        productItemInfo.put("price", product.getPrice());
        productItemInfo.put("stock", product.getStock());
        productItemInfo.put("views", product.getViews());
        productItemInfo.put("launchDate", product.getLaunchDate());
        productItemInfo.put("salesVolume", product.getSalesVolume());
        return productItemInfo;
    }

    /***
     * @Description: 添加商品页面显示
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @GetMapping("/product/Add")
    public String productDataView1(Model model, HttpSession session) {
        model.addAttribute("userName", session.getAttribute("userName"));
        return "/product/Add";
    }

    /***
     * @Description: 添加商品信息
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @PostMapping("/product/addProduct")
    public String addProduct(@RequestParam("productName") String productName,
                             @RequestParam("price") BigDecimal price,
                             @RequestParam("stock") Integer stock,
                             @RequestParam("description") String description,
                             @RequestParam("imageUrl") MultipartFile imageUrl,
                             Model model,
                             HttpSession session) {
        // 创建ProductDTO对象
        ProductDTO productDTO = new ProductDTO();
        productDTO.setProductName(productName);
        productDTO.setPrice(price);
        productDTO.setStock(stock);
        productDTO.setDescription(description);
        Products products = new Products();
        BeanUtil.copyProperties(productDTO, products);
        products.setStatus(true);
        products.setViews(0);
        products.setLaunchDate(LocalDate.now());
        products.setIsDeleted(false);
        products.setSalesVolume(0);
        log.info("文件上传");
        // 检查 imageUrl 是否为空
        if (imageUrl != null && !imageUrl.isEmpty()) {
            try {
                // 原始文件名
                String originalFilename = imageUrl.getOriginalFilename();
                // 截取文件名后缀  xxx.png
                assert originalFilename != null;
                String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
                // 构造新文件名称
                String objectName = UUID.randomUUID() + extension;
                // 返回文件请求路径
                String filePath = aliOssUtil.upload(imageUrl.getBytes(), objectName);
                products.setImageUrl(filePath);
            } catch (IOException e) {
                log.error("文件上传失败");
                e.printStackTrace();
            }
        }
        boolean added = productsService.save(products);
        if (added) {
            log.info("商品添加成功");
            return "redirect:/product/List";
        } else {
            model.addAttribute("error", "商品添加失败");
            log.error("商品添加失败");
            return "redirect:/product/Add";
        }
    }

    /***
     * @Description: 删除商品
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @GetMapping("/deletep")
    public String delete(@RequestParam(value = "id") Long id) {
        productsMapper.deleteById(id);
        log.info("删除用户信息：{}", id);
        return "redirect:/product/List";
    }

    /***
     * @Description: 批量删除商品信息
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @DeleteMapping("/deleteBatch")
    public ResponseEntity<?> deleteBatch(@RequestParam("ids") List<String> ids) {
        try {
            productsMapper.deleteBatchIds(ids);
            return ResponseEntity.ok().body("批量删除商品成功");
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    /***
     * @Description: 编辑页面显示
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @GetMapping("/editInfo")
    public String productEdit(@RequestParam(value = "id") Long id, Model model, HttpSession session) {
        Products products = productsMapper.selectById(id);
        model.addAttribute("product", products);
        model.addAttribute("userName", session.getAttribute("userName"));
        return "/product/Edit";
    }

    /***
     * @Description: 编辑商品前商品数据的回显
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */

    @GetMapping("/updateProduct")
    public String Editinfo(@RequestParam(value = "id") Long id, Model model, HttpSession session) {
        Products products = productsMapper.selectById(id);
        model.addAttribute("product", products);
        model.addAttribute("userName", session.getAttribute("userName"));
        return "redirect:/product/List";
    }

    /***
     * @Description: 更新商品
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @PostMapping("/updateProduct")
    public String updateProduct(@RequestParam("productName") String productName,
                                @RequestParam("price") BigDecimal price,
                                @RequestParam("stock") Integer stock,
                                @RequestParam("description") String description,
                                @RequestParam("imageUrl") MultipartFile imageUrl,
                                @RequestParam("productId") Long productId,
                                Model model,
                                HttpSession session) {
        Products products = new Products();
        products.setProductName(productName);
        products.setPrice(price);
        products.setStock(stock);
        products.setDescription(description);

        // 检查 imageUrl 是否为空
        if (imageUrl != null && !imageUrl.isEmpty()) {
            try {
                // 原始文件名
                String originalFilename = imageUrl.getOriginalFilename();
                // 截取文件名后缀  xxx.png
                assert originalFilename != null;
                String extension = originalFilename.substring(originalFilename.lastIndexOf("."));
                // 构造新文件名称
                String objectName = UUID.randomUUID() + extension;
                // 返回文件请求路径
                String filePath = aliOssUtil.upload(imageUrl.getBytes(), objectName);
                products.setImageUrl(filePath);
            } catch (IOException e) {
                log.error("商品图片上传失败");
                e.printStackTrace();
            }
        }
        products.setId(Math.toIntExact(productId));
        boolean updated = productsService.updateById(products);

        if (updated) {
            log.info("商品信息修改成功");
            return "redirect:/product/List";
        } else {
            model.addAttribute("error", "商品修改失败");
            log.error("商品修改失败");
            return "redirect:/product/Edit";
        }
    }

    /***
     * @Description: 商品信息的的导出
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @GetMapping("/export")
    public void export(HttpServletResponse response) throws IOException {
        String excelName = "商品信息";
        List<Products> list = productsMapper.selectList(null);
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String fileName = excelName + now.format(formatter);
        log.info("导出商品信息文件：{}", fileName);
        ExcelUtils.exportExcel(new ArrayList<>(list), excelName, excelName, Products.class, fileName, response);
    }

    /***
     * @Description: 商品加入秒杀
     * @Author: Liyunlong
     * @Date: 2024/5/13
     */
    @GetMapping("/seckill")
    public String seckill(@RequestParam(value = "id") Long id, Model model, HttpSession session) {
        Products products = productsMapper.selectById(id);
        model.addAttribute("product", products);
        model.addAttribute("userName", session.getAttribute("userName"));
        String seckillproduct = JSONUtil.toJsonStr(products);
        int randomMinutes = new Random().nextInt(30) + 1; // 生成1到30的随机整数加到时间上 防止大量商品信息缓存同时失效
        long totalExpiration = 1L + TimeUnit.DAYS.toSeconds(1) + TimeUnit.MINUTES.toSeconds(randomMinutes);
        Boolean aBoolean = redisTemplate.opsForValue().setIfAbsent("seckill:product:" + id, seckillproduct, totalExpiration, TimeUnit.SECONDS);
        if (aBoolean) {
            log.info("商品加入秒杀成功");
            return "redirect:/product/List";
        } else {
            log.info("商品加入秒杀失败");
            return "/product/Info";
        }
    }
}

