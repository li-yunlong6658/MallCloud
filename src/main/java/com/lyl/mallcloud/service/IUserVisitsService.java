package com.lyl.mallcloud.service;

import com.lyl.mallcloud.pojo.UserVisits;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户访问记录表 服务类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */
public interface IUserVisitsService extends IService<UserVisits> {

}
