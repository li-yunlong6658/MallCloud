package com.lyl.mallcloud.service;

import com.lyl.mallcloud.pojo.ProductViews;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 存储每个商品每次访问量的表 服务类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-12
 */
public interface IProductViewsService extends IService<ProductViews> {

}
