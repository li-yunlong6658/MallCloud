package com.lyl.mallcloud.service;

import com.lyl.mallcloud.pojo.Users;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-10
 */
public interface IUsersService extends IService<Users> {

    List<Users> findAll();

    Users findById(Long id);

    Users findByUserName(String name);
}
