package com.lyl.mallcloud.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.lyl.mallcloud.pojo.Users;
import com.lyl.mallcloud.mapper.UsersMapper;
import com.lyl.mallcloud.service.IUsersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-10
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements IUsersService {
@Autowired
UsersMapper usersMapper;
    @Override
    public List<Users> findAll() {
        return usersMapper.selectList(null);
    }

    @Override
    public Users findById(Long id) {
        return usersMapper.selectById(id);
    }

    @Override
    public Users findByUserName(String name) {
        return usersMapper.findByUserName(name);
    }


}
