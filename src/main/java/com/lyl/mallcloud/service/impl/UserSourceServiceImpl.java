package com.lyl.mallcloud.service.impl;

import com.lyl.mallcloud.pojo.UserSource;
import com.lyl.mallcloud.mapper.UserSourceMapper;
import com.lyl.mallcloud.service.IUserSourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */
@Service
public class UserSourceServiceImpl extends ServiceImpl<UserSourceMapper, UserSource> implements IUserSourceService {

}
