package com.lyl.mallcloud.service.impl;

import com.lyl.mallcloud.mapper.AdminMapper;
import com.lyl.mallcloud.pojo.Admin;
import com.lyl.mallcloud.service.IAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-10
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

    @Autowired
    AdminMapper adminMapper;

    @Override
    public Admin findByUsername(String username) {
      return   adminMapper.findByUsername(username);
    }

    @Override
    public boolean save(Admin entity) {
        return super.save(entity);
    }
}
