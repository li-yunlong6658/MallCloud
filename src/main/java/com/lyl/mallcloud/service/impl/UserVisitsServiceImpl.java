package com.lyl.mallcloud.service.impl;

import com.lyl.mallcloud.pojo.UserVisits;
import com.lyl.mallcloud.mapper.UserVisitsMapper;
import com.lyl.mallcloud.service.IUserVisitsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户访问记录表 服务实现类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */
@Service
public class UserVisitsServiceImpl extends ServiceImpl<UserVisitsMapper, UserVisits> implements IUserVisitsService {

}
