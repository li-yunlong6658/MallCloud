package com.lyl.mallcloud.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lyl.mallcloud.pojo.Products;
import com.lyl.mallcloud.mapper.ProductsMapper;
import com.lyl.mallcloud.service.IProductsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 存储所有商品信息的表 服务实现类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-11
 */
@Service
public class ProductsServiceImpl extends ServiceImpl<ProductsMapper, Products> implements IProductsService {


    @Autowired
    ProductsMapper productsMapper;

    @Override
    public IPage<Products> getProductListByPage(Integer pageNum, Integer pageSize) {
        Page<Products> page = new Page<>(pageNum, pageSize);
        return productsMapper.selectPage(page, null);
    }
}
