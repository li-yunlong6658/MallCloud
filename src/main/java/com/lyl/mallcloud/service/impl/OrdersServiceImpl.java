package com.lyl.mallcloud.service.impl;

import com.lyl.mallcloud.mapper.OrdersMapper;
import com.lyl.mallcloud.pojo.Orders;
import com.lyl.mallcloud.service.IOrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 存储所有订单信息的表 服务实现类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-12
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements IOrdersService {

    @Autowired
    OrdersMapper ordersMapper;

    @Override
    public List<Integer> getTopFiveProductIdsBySales() {
        List<Map<String, Object>> resultList = ordersMapper.getTopFiveProductIdsBySalesVolume();
        return resultList.stream()
                .map(map -> map.get("product_id") instanceof Integer ? (Integer) map.get("product_id") : null)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
