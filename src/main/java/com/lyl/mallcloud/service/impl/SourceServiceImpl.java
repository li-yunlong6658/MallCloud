package com.lyl.mallcloud.service.impl;

import com.lyl.mallcloud.pojo.Source;
import com.lyl.mallcloud.mapper.SourceMapper;
import com.lyl.mallcloud.service.ISourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */
@Service
public class SourceServiceImpl extends ServiceImpl<SourceMapper, Source> implements ISourceService {

}
