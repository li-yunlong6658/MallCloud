package com.lyl.mallcloud.service.impl;

import com.lyl.mallcloud.pojo.ProductViews;
import com.lyl.mallcloud.mapper.ProductViewsMapper;
import com.lyl.mallcloud.service.IProductViewsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 存储每个商品每次访问量的表 服务实现类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-12
 */
@Service
public class ProductViewsServiceImpl extends ServiceImpl<ProductViewsMapper, ProductViews> implements IProductViewsService {

}
