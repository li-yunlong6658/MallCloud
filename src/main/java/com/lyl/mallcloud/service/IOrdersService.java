package com.lyl.mallcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyl.mallcloud.pojo.Orders;

import java.util.List;

/**
 * <p>
 * 存储所有订单信息的表 服务类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-12
 */
public interface IOrdersService extends IService<Orders> {

    List<Integer> getTopFiveProductIdsBySales();
}
