package com.lyl.mallcloud.service;

import com.lyl.mallcloud.pojo.Source;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */
public interface ISourceService extends IService<Source> {

}
