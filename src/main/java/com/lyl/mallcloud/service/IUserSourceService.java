package com.lyl.mallcloud.service;

import com.lyl.mallcloud.pojo.UserSource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-14
 */
public interface IUserSourceService extends IService<UserSource> {

}
