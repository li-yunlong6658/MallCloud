package com.lyl.mallcloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyl.mallcloud.pojo.Admin;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-10
 */
public interface IAdminService extends IService<Admin> {

    Admin findByUsername(String username);
}
