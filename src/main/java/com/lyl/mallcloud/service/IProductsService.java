package com.lyl.mallcloud.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lyl.mallcloud.pojo.ProductDTO;
import com.lyl.mallcloud.pojo.Products;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 存储所有商品信息的表 服务类
 * </p>
 *
 * @author liyunlong
 * @since 2024-05-11
 */
public interface IProductsService extends IService<Products> {

    IPage<Products> getProductListByPage(Integer pageNum, Integer pageSize);
}
