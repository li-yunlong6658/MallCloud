package com.lyl.mallcloud.config;

import com.lyl.mallcloud.mapper.AdminMapper;
import com.lyl.mallcloud.pojo.Admin;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

/***
* @Description: 
* @Author: Liyunlong
* @Date: 2024/5/10
*/
public class MyRealm extends AuthorizingRealm {

    @Autowired
    AdminMapper adminMapper;
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }
    @Override //认证
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
       //从被shiro封装成的token中取出我们传入的username
        String username = (String) authenticationToken.getPrincipal();
        Admin byUsername = adminMapper.findByUsername(username);
        if(byUsername == null){
            throw new UnknownAccountException("账号不存在");
        }
        String username1 = byUsername.getUsername();
        String password = byUsername.getPassword();
        String salt = byUsername.getSalt();
        if (!username.equals(username1)){
            throw new IncorrectCredentialsException("密码错误");
        }
        return new SimpleAuthenticationInfo(username, password, this.getName());
    }
}
