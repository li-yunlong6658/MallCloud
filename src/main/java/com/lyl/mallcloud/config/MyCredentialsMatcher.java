package com.lyl.mallcloud.config;

import com.lyl.mallcloud.mapper.AdminMapper;
import com.lyl.mallcloud.pojo.Admin;
import com.lyl.mallcloud.utils.Md5Util;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.springframework.beans.factory.annotation.Autowired;

/***
* @Description: 密码校验
* @Author: Liyunlong
* @Date: 2024/5/12
*/
public class MyCredentialsMatcher extends SimpleCredentialsMatcher {

    @Autowired
    AdminMapper adminMapper;
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        UsernamePasswordToken tokenResolve = (UsernamePasswordToken) token;
        String tokenPwd = new String(tokenResolve.getPassword());
        String username = tokenResolve.getUsername();
        Admin byUsername = adminMapper.findByUsername(username);
        String salt = byUsername.getSalt();
        String md5String = Md5Util.getMD5String(tokenPwd + salt);
        String infoPwd =(String) info.getCredentials();
        return super.equals(md5String, infoPwd);
    }
}
