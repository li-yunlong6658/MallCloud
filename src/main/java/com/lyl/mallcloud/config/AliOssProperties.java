package com.lyl.mallcloud.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
/**
 * @ClassName AliOssProperties
 * @Description 阿里云oss属性配置类
 * @Version 1.0
 **/

// 阿里云oss属性配置类
@Component
@ConfigurationProperties(prefix = "lyl.alioss")
@Data
public class AliOssProperties {

    private String endpoint;
    private String accessKeyId;
    private String accessKeySecret;
    private String bucketName;

}
