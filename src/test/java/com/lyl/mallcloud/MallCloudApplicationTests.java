package com.lyl.mallcloud;

import cn.hutool.json.JSONUtil;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lyl.mallcloud.mapper.*;
import com.lyl.mallcloud.pojo.*;
import com.lyl.mallcloud.service.ISourceService;
import com.lyl.mallcloud.service.IUserVisitsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.io.FileInputStream;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@SpringBootTest
class MallCloudApplicationTests {

    @Autowired
    UsersMapper userMapper;

    @Autowired
    com.lyl.mallcloud.mapper.ProductsMapper productsMapper;

    @Autowired
    OrdersMapper ordersMapper;


    @Autowired
    SourceMapper sourceMapper;

    @Test
    void contextLoads() {
        // 创建一个QueryWrapper实例并设置排序条件，按照salesVolume字段降序排序
        QueryWrapper<Products> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("sales_volume");

        List<Products> productList = productsMapper.selectList(queryWrapper);

        List<Map<String, Object>> productItemList = productList.stream()
                .map(product -> {
                    Map<String, Object> productItemInfo = new HashMap<>();
                    productItemInfo.put("id", product.getId());
                    productItemInfo.put("productName", product.getProductName());
                    productItemInfo.put("price", product.getPrice());
                    productItemInfo.put("stock", product.getStock());
                    productItemInfo.put("views", product.getViews());
                    productItemInfo.put("launchDate", product.getLaunchDate());
                    productItemInfo.put("salesVolume", product.getSalesVolume());
                    return productItemInfo;
                })
                .collect(Collectors.toList());

        System.out.println(productItemList);
    }

    @Test
    public void testDelete() {
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = "https://oss-cn-beijing.aliyuncs.com";
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = "LTAI5t7E4rgeQcTQrz36GbRm";
        String accessKeySecret = "r3IRIhfYBqzePlk1sCGRIrpc2qdaXZ";
        // 填写Bucket名称，例如examplebucket。
        String bucketName = "sky-liyunlong";
        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
        String objectName = "0001.jpg";
        // 填写本地文件的完整路径，例如D:\\localpath\\examplefile.txt。
        // 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
        String filePath = "C:\\Users\\32310\\Desktop\\李云龙\\PICT\\照片\\证书\\录取通知书.jpg";

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            InputStream inputStream = new FileInputStream(filePath);
            // 创建PutObject请求。
            ossClient.putObject(bucketName, objectName, inputStream);
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (Exception ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    // @Test
/*    public void testUpload() {
        List<Map<String, Object>> rawCounts = productsMapper.countVisits();
        Map<String, Object> map = rawCounts.get(0);
        System.out.println(map.get("view_range"));
        System.out.println(map.get("count"));
        // int count = (int) map.get("count");
        // System.out.println(count);
        System.out.println(rawCounts);
    }*/

    @Test
    void test2() {
        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        ordersQueryWrapper.orderByAsc("order_date");
        List<Orders> orders = ordersMapper.selectList(ordersQueryWrapper);

        LocalDateTime[] localDateTimes = orders.stream()
                .map(Orders::getOrderDate)
                .toArray(LocalDateTime[]::new);

        // 提取月份并打印
        Arrays.stream(localDateTimes)
                .forEach(date -> System.out.println(date.getMonthValue()));
    }

    @Test
    void test3() {
        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        ordersQueryWrapper.orderByAsc("order_date");
        List<Orders> orders = ordersMapper.selectList(ordersQueryWrapper);
        // 将月份转换为LocalDateTime并按月份排序

        // 分组并计算销售量总和
        Map<Integer, Integer> monthlySales = orders.stream()
                .collect(Collectors.groupingBy(order -> order.getOrderDate().getMonthValue(),
                        Collectors.summingInt(Orders::getQuantity)));
        // 将销售量值存入数组
        int[] salesValues = new int[monthlySales.size()];
        int index = 0;
        for (int salesTotal : monthlySales.values()) {
            salesValues[index++] = salesTotal;
            System.out.println(salesTotal);
        }
        System.out.println(salesValues);
    }

    @Autowired
    ProductViewsMapper productViewsMapper;

    @Test
    void test4() {
        QueryWrapper<Orders> ordersQueryWrapper = new QueryWrapper<>();
        ordersQueryWrapper.orderByAsc("order_date");
        List<Orders> orders = ordersMapper.selectList(ordersQueryWrapper);
// 查询每月的访问量
        QueryWrapper<ProductViews> viewsQueryWrapper = new QueryWrapper<>();
        viewsQueryWrapper.orderByAsc("view_time");
        List<ProductViews> views = productViewsMapper.selectList(viewsQueryWrapper);
// 提取月份并放入数组，使用 distinct() 去除重复的月份
        Integer[] months = orders.stream()
                .map(Orders::getOrderDate)
                .map(LocalDateTime::getMonthValue)
                .distinct()
                .toArray(Integer[]::new);
        for (Integer month : months) {
            System.out.println(month);
        }
    }

    @Test
    void test5() {
        // 查询按照销售量排序的商品降序排列
        QueryWrapper<Orders> salesQueryWrapper = new QueryWrapper<>();
        salesQueryWrapper.orderByDesc("quantity");
        List<Orders> salesProductList = ordersMapper.selectList(salesQueryWrapper);
        Map<Integer, Long> productQuantityMap = salesProductList.stream()
                .collect(Collectors.groupingBy(Orders::getProductId,
                        Collectors.summingLong(Orders::getQuantity)));
        // 创建一个包含商品ID和总数量的列表
        List<Map.Entry<Integer, Long>> sortedEntries = new ArrayList<>(productQuantityMap.entrySet());
        // 按照总数量进行降序排序
        sortedEntries.sort(Map.Entry.<Integer, Long>comparingByValue().reversed());

        // 分别提取ID和数量，创建新的列表
        List<Integer> productIds = sortedEntries.stream()
                .map(Map.Entry::getKey)
                .toList();
        List<Long> totalQuantities = sortedEntries.stream()
                .map(Map.Entry::getValue)
                .toList();
        List<Products> salesProductList2 = new ArrayList<>(productIds.size());
        for (int i = 0; i < productIds.size(); i++) {
            Products product = productsMapper.selectById(productIds.get(i));
            if (product != null) {
                salesProductList2.add(product);
            }

        }
        salesProductList2.forEach(System.out::println);

    }

    @Autowired
    UserSourceMapper userSourceMapper;
    @Autowired
    ISourceService sourceService;



    @Test
    void getUserSources() {
        List<Map<String, Object>> result = userSourceMapper.selectBySource();
        List<String> sourceNames = new ArrayList<>();
        List<Long> userCounts = new ArrayList<>();
        List<VisitCount> visitCountList = new ArrayList<>();

        for (Map<String, Object> row : result) {
            sourceNames.add((String) row.get("source_name"));
            userCounts.add((Long) row.get("total_users")); // 修改这里，将Integer替换为Long
            visitCountList.add(new VisitCount((String) row.get("source_name"),
                    (Long) row.get("total_users")));
        }
        VisitCounts visitCounts = new VisitCounts();
        visitCounts.setLegendData(sourceNames);
        visitCounts.setData(visitCountList);
        System.out.println(visitCounts);
    }

    @Autowired
  VisitDatumMapper visitDatumMapper;
    @Test
    void getUserSources2() {
        List<VisitDatum> weeklyVisitData = visitDatumMapper.getWeeklyVisitData();
        System.out.println(weeklyVisitData);
    }

}
